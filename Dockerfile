FROM python:3.8.5-alpine

MAINTAINER Darlan Ullmann <dullmann@universo.univates.br>

RUN pip install Flask

COPY . /opt/server/

WORKDIR /opt/server/

EXPOSE 80

CMD [ "python", "server.py" ]
