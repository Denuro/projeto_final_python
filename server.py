from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html", message="Programação de Redes com Python!")
    
@app.route("/upper", methods=["POST"])
def upper():
    return jsonify({"message": request.json["message"].upper()})

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)
